# Freefy API [1.0.0] [OAS3]

[https://app.freefy.online/swagger.yaml](https://app.freefy.online/swagger.yaml)

Access token should be sent along with every request to Freefy API in authorization header:  `Authorization: Bearer <Token>`  
This token can either be acquired via  `auth/login`  endpoint or from account settings page on  [Freefy app](https://app.freefy.online/account/settings).

## Servers
https://app.freefy.online/api/v1

## Search

## Playlists

## Artists

## Albums

## Tracks

## Genres

## Tags

## Users

## Uploads (Upload files to Freefy)

## Chunked uploads (Upload files using chunked uploading to Freefy)

## Auth (Authenticate rEquest to the Api)

## Files & folders (FileEntry refers to either a file or folder on BeMusic)

## Sharing (Manage file entry collaborators)

## Starring (Mark or unmark entries as starred)

## Shareable links (Manage shareable links for file or folder)

# Schemas

 - Pagination

    enter code here

 - Artist
 - Album
 - Track
 - Playlist
 - Genre
 - Comment
 - Lyric
 - Profile
 - ProfileImage
 - User
 - Tag
 - 401 Response
 - 403 Response
 - 404 Response
 - 422 Response
